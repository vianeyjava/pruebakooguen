import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../../services/users/users.service';
import { ModelUser } from '../../shared/model_class/ModelUser';
// Declaramos las variables para jQuery
declare var jQuery:any;
declare var $:any;


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  userId: any;
  idempresa: any;
  public routerId: any;
  public listUsers: ModelUser[] = [];
  public modelUser: ModelUser;

  constructor(private _userService: UsersService,
    private _activateRouter: ActivatedRoute,
    private router: Router,) { 
      this.modelUser = new ModelUser();
    }

    ngOnInit(): void {
      // this.getUserId();
      this.findAllUsers();
    }
    // getUserId() {
    //   this._activateRouter.params.subscribe(params => {
    //     this.routerId = params['id'];
    //     if (this.routerId !=null) {
    //       this._userService.getUsersId(this.routerId).subscribe(respData => {
    //         this.modelUser = respData;
    //       });
    //     }
    //   });
    // }
    findAllUsers() {
      this._userService.findAllUsers().subscribe(data => {
        this.listUsers = data;
        console.table(this.listUsers);
      });
    }

    deleteUser(id:any) {
      this._userService.delete(id).subscribe(data => {
        this.findAllUsers();
      });
    }

    searchHeadquarter(){
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    }
  selectRow(row: any){
    this.router.navigate([`/register-user/edit/${row.id}`]);
  }
  


}
