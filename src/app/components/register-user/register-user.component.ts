import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users/users.service';
import { ModelTipo } from 'src/app/shared/model_class/ModelTipo';
import { ModelUser } from 'src/app/shared/model_class/ModelUser';
import { UserTypeService } from '../../services/user_type/user-type.service';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

  cod: any;
  myForm: FormGroup;
  public modelUser: ModelUser;
  public modelTypeUser: ModelTipo;
  public listTypeUser: ModelTipo[] = [];
  edicion: boolean = false;
  public routerId: any;


  constructor(
    private _formBuilder: FormBuilder,
    private _activateRouter: ActivatedRoute,
    private __serviceTypeService: UserTypeService,
    private __serviceUsers: UsersService,
    private router: Router,) {
    this.modelUser = new ModelUser();
    this.modelTypeUser = new ModelTipo();
    this.myForm = this._formBuilder.group({
      'id': ['', [Validators.required, Validators.minLength(1)]],
      'nombre': ['', [Validators.required, Validators.minLength(5)]],
      'apellido': ['', []],
      'email': ['', [Validators.required, Validators.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      'usuario': ['', [Validators.required, Validators.minLength(1)]],
      'password': ['', [Validators.required, Validators.minLength(5)]],
      'tipoDeUsuario': ['', [Validators.required]],
    });
    /*this.myForm = new FormGroup({
      id: new FormControl('', Validators.required),
      nombre: new FormControl(['', [Validators.required,]]),
      apellido: new FormControl(['', []]),
      email: new FormControl(['', [Validators.required, Validators.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]]),
      usuario: new FormControl(['', [Validators.required,]]),
      password: new FormControl(['', [Validators.required,]]),
      tipoDeUsuario: new FormControl(['', [Validators.required,]]),
    });*/
  }


  ngOnInit(): void {
    this.getAllTypeUser();
    this.getUserId();
  }

  getUserId() {
    this._activateRouter.params.subscribe(params => {
      this.routerId = params['id'];
      if (this.routerId != null) {
        this.__serviceUsers.getUsersId(this.routerId).subscribe(resp => {
          this.modelUser = resp;
          this.edicion = params['id'] != null;
          console.log('PARAMETROS ', resp);
          this.initForm();
        });
      }
    });
  }
  getAllTypeUser() {
    this.__serviceTypeService.findTypeUsers().subscribe(data => {
      this.listTypeUser = data;
    });
  }
  compare(objet1: ModelTipo, objet2: ModelTipo): boolean {
    if (objet1 === undefined && objet2 === undefined) {
      return true;
    }
    return objet1 === null || objet2 === null || objet1 === undefined || objet2 === undefined ? false : objet1.id === objet2.id;
  }

  saveData() {
    this.modelUser.id = this.myForm.value['id']
    this.modelUser.nombre = this.myForm.value['nombre'];
    this.modelUser.apellido = this.myForm.value['apellido'];
    this.modelUser.email = this.myForm.value['email'];
    this.modelUser.usuario = this.myForm.value['usuario'];
    this.modelUser.password = this.myForm.value['password'];
    this.modelUser.tipoDeUsuario = this.myForm.value['tipoDeUsuario'];
    if (this.edicion) {
      this.__serviceUsers.update(this.modelUser).subscribe(resp => {
        alert('Usuario Modificado');
        this.router.navigate([`/users`]);
      });
    } else {
      this.__serviceUsers.create(this.modelUser).subscribe(data => {
        alert('Usuario registrado');
        this.router.navigate([`/users`]);
      });
    }


  }

  public initForm() {
    if (this.edicion) {
      this.__serviceUsers.getUsersId(this.routerId).subscribe(data => {
        let id = data.id;
        let nombre = data.nombre;
        let apellido = data.apellido;
        let email = data.email;
        let usuario = data.usuario;
        let password = data.password;
        let tipoUsuairo = data.tipoDeUsuario;
        this.myForm = this._formBuilder.group({
          'id': [id, [Validators.required, Validators.minLength(1)]],
          'nombre': [nombre, [Validators.required, Validators.minLength(5)]],
          'apellido': [apellido, []],
          'email': [email, [Validators.required, Validators.email]],
          'usuario': [usuario, [Validators.required]],
          'password': [password, [Validators.required]],
          'tipoDeUsuario': [tipoUsuairo, [Validators.required]],
        });
        console.log('DATA INITFORM: ', usuario);
      });
    }
  }
}
