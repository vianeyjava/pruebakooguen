import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import {RouterModule} from "@angular/router";
import { CommentsComponent } from './components/comments/comments.component';
import { TipoUsuarioComponent } from './components/tipo-usuario/tipo-usuario.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { NopagefoundComponent } from './components/nopagefound/nopagefound.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';
import { NavbarComponent } from './components/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    CommentsComponent,
    TipoUsuarioComponent,
    UsuariosComponent,
    HomeComponent,
    NopagefoundComponent,
    RegisterUserComponent,
    NavbarComponent
  ],
    imports: [
        BrowserModule,
        RouterModule,
        CommonModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        AppRoutingModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
