import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ModelTipo } from 'src/app/shared/model_class/ModelTipo';
import { HOST } from 'src/app/shared/var.constant';

@Injectable({
  providedIn: 'root'
})
export class UserTypeService {

  private apiUrl: string = `${HOST}`;

  constructor(public http: HttpClient) { }

  findTypeUsers(): Observable<ModelTipo[]> {
    return this.http.get<ModelTipo[]>(`${this.apiUrl}/tipoDeUsuario`);
  }
}
