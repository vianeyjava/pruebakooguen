import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModelUser } from '../../../app/shared/model_class/ModelUser';
import { of, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HOST } from '../../shared/var.constant';

const headerOption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private apiUrl: string = `${HOST}`;

  constructor(public http: HttpClient) { }

  findAllUsers(): Observable<ModelUser[]> {
    return this.http.get<ModelUser[]>(`${this.apiUrl}/employees`,headerOption);
  }

  /**
   * Función que permite crear usuarios
   * @param modelUser parametro que permite crear todo el objeto
   */
  create(modelUser: ModelUser): Observable<ModelUser> {
    return this.http.post(`${this.apiUrl}/employees`, modelUser,headerOption).pipe(
      map((response: any) => response.entityNewProduct as ModelUser),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      })
    );
  }

  /**
   * Funcion encargada para actualizar un Usuario
   * @param object
   */
  update(modelUser: ModelUser): Observable<ModelUser> {
    return this.http.put<ModelUser>(`${this.apiUrl}/employees/${modelUser.id}`, modelUser,headerOption).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }

  /**
   * Funcion encargada de elminar un usuario
   * @param id  parametro filtro para eliminar por dicho id usuario
   */
  delete(id: any): Observable<ModelUser> {
    return this.http.delete<ModelUser>(`${this.apiUrl}/employees/${id}`,headerOption).pipe(
      catchError(e => {
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }

  /**
   * Cargamos los datos de un determinado Usuario por id
   * @param id parametro que me permite consultar por id --> usuario
   */
  getUsersId(id: any): Observable<ModelUser> {
    return this.http.get<ModelUser>(`${this.apiUrl}/employees/${id}`,headerOption).pipe(
      catchError(e => {

        console.error(e.error.mensaje);


        return throwError(e);
      }));
  }
}
