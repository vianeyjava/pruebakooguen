import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { NopagefoundComponent } from './components/nopagefound/nopagefound.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { RegisterUserComponent } from './components/register-user/register-user.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'users', component: UsuariosComponent},
  { path: 'register-user/new', component: RegisterUserComponent},
  { path: 'register-user/edit/:id', component: RegisterUserComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: NopagefoundComponent },
  
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    CommonModule,
    RouterModule
  ]
})
export class AppRoutingModule { }
