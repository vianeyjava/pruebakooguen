import { ITipo } from "../models/tipo.model";

export class ModelTipo implements ITipo {

    id: number | string;
    nombre: string;
    descripcion: string;

    constructor() {
       
    }
    
}