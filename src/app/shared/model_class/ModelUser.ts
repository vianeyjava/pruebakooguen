
import { ModelTipo } from "./ModelTipo";
import { IUsuario } from "../models/usuario.model";
import { Optional } from "@angular/core";

export class ModelUser implements IUsuario {

    public id: string | number | null;
    public usuario: string;
    public password: string;
    public tipoDeUsuario: ModelTipo;
    public nombre: string;
    public apellido: string;
    public email: string;


    // constructor() {
    //     this.tipoDeUsuario = new ModelTipo();
    // }
    constructor(values: Object = {}) {
        //Constructor initialization
        Object.assign(this, values);
    }

}
