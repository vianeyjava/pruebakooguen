import { ModelTipo } from "../model_class/ModelTipo";
import {ITipo} from "./tipo.model";

export interface IUsuario {
  // id: string|number|null;
  // usuario: string;
  // password: string;
  // tipoDeUsuario: ITipo;

   id: string | number | null;
   usuario: string;
   password: string;
   tipoDeUsuario: ITipo;
   nombre: string;
   apellido: string;
   email: string;

   
  
}
